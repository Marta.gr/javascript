function EscribirDias() {
	var  tablaDias,
		 contados,
		 resultadoDias;

	tablaDias = new Array ();

	tablaDias[0] = "Lunes";

	tablaDias[1] = "Martes";

	tablaDias[2] = "Miercoles";

	tablaDias[3] = "Jueves";

	tablaDias[4] = "Viernes";


	resultadoDias = document.getElementById('resultadoDias');

	//Recorrer tablaDias con For

	for (contador=0; contador<5; contador++)
	{
		resultadoDias.innerHTML += "<br> " + tablaDias[contador]; 
	}

	//Recorrer tabladias con For In

	for(contador in tablaDias)
	{
		resultadoDias.innerHTML += "<br> " + tablaDias[contador];
	}
}